package edu.hope.cs.csci392.imdb;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;


public class RequestRouter {
	

	public static class RequestInfo {
		public Pattern urlPattern;
		public String servletName;
		public String[] requestAttributeNames;
		
		public RequestInfo (String pattern, String servlet) {
			this (pattern, servlet, new String[0]);
		}
		
		public RequestInfo (String pattern, String servlet, String[] requestAttributeNames) {
			urlPattern = Pattern.compile(pattern);
			servletName = servlet;			
			this.requestAttributeNames = new String[requestAttributeNames.length];
			System.arraycopy(requestAttributeNames, 0, this.requestAttributeNames, 0, requestAttributeNames.length);			
		}
	}

	private static final RequestInfo[] mappings = {
		new RequestInfo ("/MovieSearch", "MovieSearch"),
		new RequestInfo ("/ActorSearch", "ActorSearch"),
		new RequestInfo ("/Genres", "GenreSearch")
	};
	
	public RequestInfo getServletForRequest (HttpServletRequest request) {
		String pathInfo = request.getPathInfo();
		
		for (RequestInfo mapping : mappings) {
			Matcher matcher = mapping.urlPattern.matcher(pathInfo);
			if (matcher.matches()) {
				for (int i = 0; i < mapping.requestAttributeNames.length; i++) {
					request.setAttribute(mapping.requestAttributeNames[i], matcher.group(i+1));
				}
				return mapping;
			}
		}
		
		return null;
	}
}
